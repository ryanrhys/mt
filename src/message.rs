#![allow(unused)]
#[derive(Debug)]
pub enum Message {
    CloseTab(Option<u32>),

    UpdateWindowTitle(String),
    UpdateTabTitle(Option<u32>, String),

    // Clipboard
    CopyToClipboard(Option<u32>),
    PasteToTerminal(Option<u32>),

    //etc
    UpdateGeometryHints(u32),
}

#[derive(Debug)]
pub enum WindowCommand {
    CloseWindow,
    CloseTab(u32),
    CloseActiveTab,
    UpdateWindowGeometry(u32),

    Clipboard {
        page: u32,
        action: Clipboard,
    },

    UpdateWindowTitle(String),
    UpdateTabTitle {
        page: u32,
        title: String,
    },
}

#[derive(Debug)]
pub enum ApplicationCommand {
    Quit,
    CreateWindow,
}

#[derive(Debug)]
pub enum Clipboard {
    Paste,
    Copy,
}