use crate::error::*;

use std::{
    io::{Error, ErrorKind},
    path::{Path, PathBuf},
};

pub trait Load {
    fn load_from_path(path: &PathBuf) -> Result<Self>
    where
        Self: Sized;
}

pub trait LoadMtResource<P>
where
    P: AsRef<Path> + Copy,
{
    fn load_data<T: Load>(&self, p: P) -> Result<T>;
    fn load_data_or_default<T: Load + Default>(&self, p: P) -> T;
    fn load_config_or_default<T: Load + Default>(&self, p: P) -> T;
}

impl<P> LoadMtResource<P> for xdg::BaseDirectories
where
    P: AsRef<Path> + Copy,
{
    fn load_data<T: Load>(&self, p: P) -> Result<T> {
        self.find_data_file(p)
            .ok_or(
                Error::new(
                    ErrorKind::NotFound,
                    "Data not found in XDG base directories",
                )
                .into(),
            )
            .and_then(|p| {
                log::info!("File found on disk, attempting to load");
                T::load_from_path(&p)
            })
    }

    fn load_data_or_default<T: Load + Default>(&self, p: P) -> T {
        load_with_func(self, p, |xdg, p| xdg.find_data_file(p))
    }

    fn load_config_or_default<T: Load + Default>(&self, p: P) -> T {
        load_with_func(self, p, |xdg, p| xdg.find_config_file(p))
    }
}

fn load_with_func<T, P, F>(xdg: &xdg::BaseDirectories, p: P, f: F) -> T
where
    T: Load + Default,
    P: AsRef<Path> + Copy,
    F: Fn(&xdg::BaseDirectories, P) -> Option<PathBuf>,
{
    f(xdg, p)
        .and_then(|p| {
            log::info!("File found on disk, attempting to load");
            T::load_from_path(&p)
                .and_then(|c| {
                    log::info!("Successfully loaded from {}", p.to_string_lossy());
                    Ok(c)
                })
                .or_else::<Error, _>(|e| {
                    log::warn!("Failed to load from disk {}, defaulting", e);
                    Ok(T::default())
                })
                .ok()
        })
        .or_else(|| {
            log::info!(
                "{} not found on disk, defaulting",
                p.as_ref().to_string_lossy()
            );
            Some(T::default())
        })
        .unwrap()
}
