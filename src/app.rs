use crate::{
    config::{ColorScheme, Config, Profile, ScrollbarVisibility},
    traits::*,
    util,
    window::{self, MtApplicationWindow},
};

use gio::{prelude::*, ApplicationCommandLineExt, ApplicationFlags, ResourceLookupFlags};
use glib::{
    char::Char,
    clone, subclass,
    subclass::prelude::*,
    translate::{FromGlibPtrFull, ToGlib, ToGlibPtr},
    OptionArg, OptionFlags, VariantTy,
};
use gdk::Screen;
use gtk::{prelude::*, subclass::prelude::*, CssProvider, Settings, SettingsExt, StyleContext, Window};
use log::debug;
use once_cell::unsync::OnceCell;
use std::{cell::RefCell, path::PathBuf,  str, rc::Rc};
use xdg::BaseDirectories;

#[cfg(debug_assertions)]
pub static APPID: &'static str = "com.gitlab.miridyan.Mt.Devel";
#[cfg(not(debug_assertions))]
pub static APPID: &'static str = "com.gitlab.miridyan.Mt";

#[derive(Clone, Debug)]
pub struct MtApplicationPriv {
    // Need to replace this is a vector for multiwindow support
    window: OnceCell<MtApplicationWindow>,
    settings: Settings,
    base_directories: BaseDirectories,
    config: Rc<RefCell<Config>>,
}

#[derive(Debug)]
pub struct TerminalParams(pub Profile, pub ColorScheme, pub ScrollbarVisibility);

impl ObjectSubclass for MtApplicationPriv {
    const NAME: &'static str = "MtApplication";

    type ParentType = gtk::Application;
    type Class = subclass::simple::ClassStruct<Self>;
    type Instance = subclass::simple::InstanceStruct<Self>;
    glib::glib_object_subclass!();

    fn new() -> Self {
        let (config, base_directories) = util::initialize_environment();
        log::info!("Config {:#?}", config);
        Self {
            window: OnceCell::new(),
            settings: Settings::get_default().unwrap(),
            base_directories,
            config: Rc::new(RefCell::new(config)),
        }
    }
}

impl ObjectImpl for MtApplicationPriv {
    glib::glib_object_impl!();
}

impl ApplicationImpl for MtApplicationPriv {
    fn activate(&self, _application: &gio::Application) {
        let _profile = self
            .config
            .try_borrow()
            .ok()
            .and_then(|config| Some(config.default_profile.clone()))
            .and_then(|profile| Some(profile.to_variant()));

        let _app = ObjectSubclass::get_instance(self)
            .downcast::<gtk::Application>()
            .unwrap();
        self.settings
            .set_property_gtk_application_prefer_dark_theme(true);
        if let Err(e) = self.load_stylesheets() {
            log::error!("Failed to load stylesheets {:?}", e);
        }
        // app.lookup_action("update-stylesheet")
        //     .unwrap()
        //     .activate(profile.as_ref());

        if let Some(styles) = self.build_stylesheets() {
            log::info!("{}", styles)
        }
    }

    fn startup(&self, application: &gio::Application) {
        self.parent_startup(application);

        if cfg!(debug_assertions) {
            Window::set_interactive_debugging(true);
        }

        if let None = self.window.get() {
            let app = ObjectSubclass::get_instance(self)
                .downcast::<MtApplication>()
                .unwrap();

            let window = MtApplicationWindow::new(&app);

            self.window.set(window).unwrap();
            app.setup_gactions();
        }
    }

    fn handle_local_options(
        &self,
        application: &gio::Application,
        options: &glib::VariantDict,
    ) -> i32 {
        if let Some(variant) =
            options.lookup_value("new-process", Some(&VariantTy::new("b").unwrap()))
        {
            if let Some(new_process) = variant.get::<bool>() {
                if new_process {
                    application.set_flags(application.get_flags() | ApplicationFlags::NON_UNIQUE);
                }
            } else {
                return 1;
            }
        }

        if let Some(variant) = options.lookup_value("config", Some(&VariantTy::new("s").unwrap())) {
            if let Some(config) = variant.get::<String>() {
                let pathbuf = PathBuf::from(config);
                let config = self.config.try_borrow_mut();

                if pathbuf.is_file() && config.is_ok() {
                    let res = Config::load_from_path(&pathbuf).and_then(|c| {
                        *config? = c;

                        Ok(())
                    });

                    match res {
                        Ok(_) => log::info!("Successfully found and parsed config"),
                        Err(e) => log::error!("Invalid config : {}", e),
                    }
                }
            }
        }

        self.parent_handle_local_options(application, options)
    }

    fn command_line(
        &self,
        application: &gio::Application,
        command_line: &gio::ApplicationCommandLine,
    ) -> i32 {
        self.activate(application);

        if let Some(options_dict) = command_line.get_options_dict() {
            let s = Some(VariantTy::new("s").unwrap());
            let cwd = options_dict
                .lookup_value("working-directory", s)
                .and_then(|option| option.get::<String>())
                .and_then(|path| util::parse_path(path));

            let cmd = options_dict
                .lookup_value("command", s)
                .and_then(|option| option.get::<String>())
                .and_then(|cmd| util::parse_cmd(cmd))
                .and_then(|p_cmd| Some(p_cmd.split(" ").map(PathBuf::from).collect::<Vec<_>>()));

            let profile = options_dict
                .lookup_value("profile", s)
                .and_then(|option| option.get::<String>());

            self.new_session(profile.as_ref().map(String::as_str), cwd, cmd);
        } else {
            self.new_session(None, None, None);
        }

        let window = self
            .window()
            .expect("Failed to get window, should not occur");

        window.show();
        window.present();
        window.focus_active_tab();

        0
    }
}

impl GtkApplicationImpl for MtApplicationPriv {}

impl MtApplicationPriv {
    pub fn window(&self) -> Option<&MtApplicationWindow> {
        self.window.get()
    }

    fn load_stylesheets(&self) -> crate::error::Result<()> {
        let provider = CssProvider::new();
        let screen = Screen::get_default().unwrap();
        let mut stylesheet = str::from_utf8(&*gio::resources_lookup_data(
            "/com/gitlab/miridyan/Mt/theme/base.css",
            ResourceLookupFlags::NONE,
        )?)?
        .to_string();
    
        if let Some(colorschemes) = self.build_stylesheets() {
            stylesheet.push_str("\n\n");
            stylesheet.push_str(&colorschemes);
        }
    
        provider.load_from_data(stylesheet.as_bytes())?;
        StyleContext::add_provider_for_screen(
            &screen,
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );
    
        Ok(())
    } 

    fn build_stylesheets(&self) -> Option<String> {
        self.config
            .try_borrow()
            .or_else(|e| {
                log::error!("Config borrow failed {:?}", e);
                Err(e)
            })
            .ok()
            .and_then(|config| {
                let stylesheet = std::str::from_utf8(
                    &*gio::resources_lookup_data(
                        "/com/gitlab/miridyan/Mt/theme/colorscheme.css",
                        ResourceLookupFlags::NONE,
                    )
                    .ok()?,
                )
                .ok()?
                .to_string();

                let colorschemes = &config.colorschemes;
                let scheme_sheets = colorschemes
                    .iter()
                    .map(|(key, val)| {
                        let tab = format!("{:06x}", val.bg >> 8);
                        let titlebar = format!("{:06x}", val.window >> 8);
                        let style_class = key.to_lowercase().replace(" ", "-");

                        stylesheet
                            .replace("{{colorscheme}}", &style_class)
                            .replace("{{tab-active}}", &tab)
                            .replace("{{titlebar}}", &titlebar)
                    })
                    .collect::<Vec<String>>()
                    .join("\n");

                Some(scheme_sheets)
            })
    }

    pub fn get_default_profile(&self) -> Option<TerminalParams> {
        let default = self
            .config
            .try_borrow()
            .or_else(|e| {
                log::error!("Config borrow failed {:?}", e);
                Err(e)
            })
            .ok()
            .and_then(|config| Some(config.default_profile.to_owned()))?;

        self.get_profile(&default)
    }

    pub fn get_profile(&self, profile_name: &str) -> Option<TerminalParams> {
        log::trace!(
            "Config strong count {:?}",
            std::rc::Rc::strong_count(&self.config)
        );
        log::trace!(
            "Config weak count {:?}",
            std::rc::Rc::weak_count(&self.config)
        );
        self.config
            .try_borrow()
            .or_else(|e| {
                log::error!("Config borrow failed {:?}", e);
                Err(e)
            })
            .ok()
            .and_then(|config| {
                let profile = config
                    .profiles
                    .profiles
                    .get(profile_name)
                    .map(|p| p.to_owned())?;

                let colorscheme = config
                    .colorschemes
                    .get(&profile.colorscheme)
                    .map(|c| c.to_owned())?;

                Some(TerminalParams(profile, colorscheme, config.scrollbar))
            })
    }

    pub fn new_session(
        &self,
        profile: Option<&str>,
        cwd: Option<PathBuf>,
        cmd: Option<Vec<PathBuf>>,
    ) {
        if let Some(window) = self.window() {
            window.terminal(profile, cwd, cmd);
        }
    }
}

glib::glib_wrapper! {
    pub struct MtApplication(
        Object<
            subclass::simple::InstanceStruct<MtApplicationPriv>,
            subclass::simple::ClassStruct<MtApplicationPriv>,
            MtApplicationClass
        >
    ) @extends gio::Application, gtk::Application;

    match fn {
        get_type => || MtApplicationPriv::get_type().to_glib(),
    }
}

impl MtApplication {
    pub fn run() {
        let app = glib::Object::new(
            Self::static_type(),
            &[
                ("application-id", &APPID.to_string()),
                ("flags", &ApplicationFlags::HANDLES_COMMAND_LINE),
            ],
        )
        .expect("Failed to create MtApplication")
        .downcast::<MtApplication>()
        .expect("Created MtApplication is of wrong type");

        app.set_default();
        app.set_resource_base_path(Some("/com/gitlab/miridyan/Mt"));

        app.add_main_option(
            "config",
            Char::new('c').unwrap(),
            OptionFlags::NONE,
            OptionArg::String,
            "Override default config path",
            Some("CONFIG_PATH"),
        );

        app.add_main_option(
            "profile",
            Char::new('p').unwrap(),
            OptionFlags::NONE,
            OptionArg::String,
            "Set the starting profile",
            Some("PROFILE_NAME"),
        );

        app.add_main_option(
            "working-directory",
            Char::new('w').unwrap(),
            OptionFlags::NONE,
            OptionArg::String,
            "Set the working directory of a session",
            Some("DIRECTORY"),
        );

        app.add_main_option(
            "command",
            Char::new('e').unwrap(),
            OptionFlags::NONE,
            OptionArg::String,
            "Set the command of a session",
            Some("COMMAND"),
        );

        app.add_main_option(
            "new-process",
            Char::new('\0').unwrap(),
            OptionFlags::NONE,
            OptionArg::None,
            "Start new instance of Mt process",
            None,
        );

        let args: Vec<String> = std::env::args().skip(0).collect();
        ApplicationExtManual::run(&app, &args);
    }

    pub fn setup_gactions(&self) {
        let app = self.clone().upcast::<gtk::Application>();
        let app_priv = MtApplicationPriv::from_instance(self);
        let window = self
            .window()
            .expect("Failed to get window, should not occur");

        self.set_accels_for_action("win.new-tab", &["<primary><shift>t"]);
        debug!("Connecting GAction: win.new-tab");

        self.set_accels_for_action("win.close-active-tab", &["<primary><shift>w"]);
        debug!("Connecting GAction: win.close-active-tab");

        self.set_accels_for_action("win.clipboard-copy", &["<primary><shift>c"]);
        debug!("Connecting GAction: win.clipboard-copy");

        self.set_accels_for_action("win.clipboard-paste", &["<primary><shift>p"]);
        debug!("Connecting GAction: win.clipboard-paste");

        util::action(
            &app,
            "quit",
            clone!(@weak app => move |_, _| {
                app.quit();
            }),
            None,
        );
        self.set_accels_for_action("app.quit", &["<primary><shift>q"]);
        debug!("Connecting GAction: app.quit");

        util::action(
            &app,
            "about",
            clone!(@weak app, @weak window => move |_, _| {
                window::about_dialog(&window, &app);
            }),
            None,
        );
        debug!("Connecting GAction: app.about");

        util::action(
            &app,
            "update-stylesheet",
            clone!(@weak app_priv.config as config => move |_, name| {
                // at some point, this will be modified such that the entire stylesheet is generated at
                // the initial run, and not loaded every time a tab is changed.
                let _ = config.try_borrow()
                    .and_then(|config| {
                        let update = name.and_then(|variant| variant.get_str())
                            .and_then(|name| config.profiles.profiles.get(name))
                            .and_then(|profile| config.colorschemes.get(&profile.colorscheme))
                            .and_then(|colorscheme| {
                                util::update_stylesheet(&colorscheme);
                                Some(())
                            });
                        log::info!("Upate stylesheet result {:?}", update);
                        Ok(())
                    });
            }),
            Some(&glib::VariantType::new("s").unwrap()),
        );
        debug!("Connecting GAction: app.update-stylesheet");

        for i in 1..11 {
            self.set_accels_for_action(
                &format!("win.select-tab-{}", i),
                &[&format!("<alt>{}", i % 10)],
            );
            debug!("Connecting GAction: win.select-tab-{}", i);
        }
    }

    pub fn window(&self) -> Option<&MtApplicationWindow> {
        let app_priv = MtApplicationPriv::from_instance(self);
        app_priv.window()
    }

    pub fn get_default_profile(&self) -> Option<TerminalParams> {
        let app_priv = MtApplicationPriv::from_instance(self);
        app_priv.get_default_profile()
    }

    pub fn get_profile(&self, profile_name: &str) -> Option<TerminalParams> {
        let app_priv = MtApplicationPriv::from_instance(self);
        app_priv.get_profile(profile_name)
    }
}
