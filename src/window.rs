// pub use preferences::{MtPreferencesWindow, MtPreferencesWindowPriv};
// pub use preferences::PreferencesWindow;

use crate::{
    app::{MtApplication, APPID},
    message::{Clipboard, WindowCommand},
    util,
    widget::{MtTabHeader, MtTerminal},
};
use gio::ApplicationExt;
use glib::{
    clone, subclass,
    subclass::prelude::*,
    translate::{FromGlibPtrFull, ToGlib, ToGlibPtr},
    MainContext, Object, Sender,
};
use gtk::{
    prelude::*, subclass::prelude::ApplicationWindowImpl as GtkApplicationWindowImpl,
    subclass::prelude::*, AboutDialogBuilder, Application,
    ApplicationWindow as GtkApplicationWindow, Box, Builder, Label, License, Notebook, Window,
};
use libhandy::{
    subclass::prelude::ApplicationWindowImpl as HdyApplicationWindowImpl,
    ApplicationWindow as HdyApplicationWindow, HeaderBar, HeaderBarExt, WindowHandle,
};
use log::debug;
use once_cell::unsync::OnceCell;
use std::{cell::RefCell, convert::TryFrom, path::PathBuf, rc::Rc};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Layout {
    // Normal,
    // Compact,
    Unified,
}

#[derive(Clone, Debug)]
pub struct Widgets {
    pub root: Box,
    pub notebook: Notebook,
    pub secondary_windowhandle: WindowHandle,
    pub secondary_titlebar: HeaderBar,
}

#[derive(Clone, Debug)]
pub struct MtApplicationWindowPriv {
    pub widgets: OnceCell<Widgets>,
    builder: OnceCell<Builder>,
    layout: Rc<RefCell<Layout>>,
    tabs_visible: RefCell<bool>,
    sender: OnceCell<Sender<WindowCommand>>,
}

impl ObjectSubclass for MtApplicationWindowPriv {
    const NAME: &'static str = "MtApplicationWindow";

    type ParentType = HdyApplicationWindow;
    type Class = subclass::simple::ClassStruct<Self>;
    type Instance = subclass::simple::InstanceStruct<Self>;
    glib::glib_object_subclass!();

    fn new() -> Self {
        Self {
            widgets: OnceCell::new(),
            builder: OnceCell::new(),
            layout: Rc::new(RefCell::new(Layout::Unified)),
            tabs_visible: RefCell::new(true),
            sender: OnceCell::new(),
        }
    }
}

impl ObjectImpl for MtApplicationWindowPriv {
    glib::glib_object_impl!();

    fn constructed(&self, obj: &Object) {
        self.parent_constructed(obj);

        let self_ = obj.downcast_ref::<MtApplicationWindow>().unwrap();
        let builder = Builder::from_resource("/com/gitlab/miridyan/Mt/ui/terminal-base.ui");
        let root = builder
            .get_object::<Box>("terminal-root")
            .expect("Unable to load terminal-root");
        let notebook = builder
            .get_object::<Notebook>("terminal-tabs")
            .expect("Unable to load terminal-notebook");
        let secondary_windowhandle = builder
            .get_object::<WindowHandle>("secondary-windowhandle")
            .expect("Unable to load secondary-windowhandle");
        let secondary_titlebar = builder
            .get_object::<HeaderBar>("secondary-titlebar")
            .expect("Unable to load secondary-titlebar");

        self_.set_title("Mt");
        self_.set_icon_name(Some(APPID));
        self_.add(&root);
        secondary_windowhandle.set_visible(false);

        let (sender, receiver) = MainContext::channel::<WindowCommand>(glib::PRIORITY_DEFAULT);
        let widgets = Widgets {
            root,
            notebook: notebook.clone(),
            secondary_windowhandle,
            secondary_titlebar,
        };

        self.builder.set(builder).expect("Failed to set builder");
        self.widgets.set(widgets).expect("Failed to set widgets");

        {
            let window = self_.clone();
            let window_priv = self.clone();
            let notebook = notebook.clone();
            receiver.attach(None, move |cmd| {
                log::debug!("Received {:?}", cmd);
                match cmd {
                    WindowCommand::UpdateWindowTitle(title) => window.set_titles(&title),
                    WindowCommand::UpdateTabTitle { page, title } => {
                        if let Some(terminal) = window_priv.get_terminal(page) {
                            if let Some(header) = notebook.get_tab_label(&terminal) {
                                if let Ok(mt_header) = header.downcast::<MtTabHeader>() {
                                    mt_header.set_title(&title);
                                }
                            }
                        }
                    }
                    WindowCommand::Clipboard { action, page } => {
                        if let Some(terminal) = window_priv.get_terminal(page) {
                            match action {
                                Clipboard::Copy => terminal.copy(),
                                Clipboard::Paste => terminal.paste(),
                            }
                        }
                    }
                    WindowCommand::UpdateWindowGeometry(page) => {
                        if let Some(terminal) = window_priv.get_terminal(page) {
                            let window = window.clone().upcast::<gtk::Window>();
                            terminal.set_geometry_hints(&window);
                        }
                    }
                    WindowCommand::CloseTab(page) => {
                        let length = notebook.get_n_pages();
                        if page < length {
                            if length == 1 {
                                debug!("Only 1 page open: quitting app");
                                window.get_application().unwrap().quit();
                            } else {
                                notebook.remove_page(Some(page));
                                debug!("Closing page {}", page);
                            }
                        } else {
                            log::warn!(
                                "Requested tab index ({}) >= size of notebook ({})",
                                page,
                                length
                            );
                        }
                    }
                    c @ _ => log::info!("Discarding command {:#?}", c),
                }
                glib::Continue(true)
            });
        }

        notebook.connect_page_removed(clone!(@weak self_ as window => move |notebook, widget, _| {
            std::mem::drop(widget);

            if notebook.get_n_pages() == 1 && window.get_tab_bar_visible() {
                window.set_tab_bar_visible(false);
            } else if notebook.get_n_pages() > 1 && !window.get_tab_bar_visible() {
                window.set_tab_bar_visible(true);
            }
        }));
        notebook.connect_switch_page(
            clone!(@strong sender, @weak self_ as window => move |_, widget, _| {
                if let Some(terminal) = widget.downcast_ref::<MtTerminal>() {
                    if let Some(title) = terminal.get_window_title() {
                        let _ = sender.send(WindowCommand::UpdateWindowTitle(title));
                        // let _ = sender.send(Message::UpdateGeometryHints(page));
                    }
                }
            }),
        );
        notebook.connect_page_added(
            clone!(@strong sender, @weak self_ as window => move |notebook, _, page| {
                notebook.set_current_page(Some(page));
                if notebook.get_n_pages() == 1 && window.get_tab_bar_visible() {
                    window.set_tab_bar_visible(false);
                } else if notebook.get_n_pages() > 1 && !window.get_tab_bar_visible() {
                    window.set_tab_bar_visible(true);
                }
            }),
        );
        self.sender.set(sender).expect("Failed to set sender");
    }
}

impl MtApplicationWindowPriv {
    pub fn notebook(&self) -> Option<&Notebook> {
        if let Some(widgets) = self.widgets.get() {
            return Some(&widgets.notebook);
        }

        None
    }

    pub fn titlebar(&self) -> Option<&HeaderBar> {
        if let Some(widgets) = self.widgets.get() {
            return Some(&widgets.secondary_titlebar);
        }

        None
    }

    pub fn windowhandle(&self) -> Option<&WindowHandle> {
        if let Some(widgets) = self.widgets.get() {
            return Some(&widgets.secondary_windowhandle);
        }

        None
    }

    pub fn create_sender(&self) -> Sender<WindowCommand> {
        self.sender.get().unwrap().clone()
    }

    pub fn get_terminal(&self, page_num: u32) -> Option<MtTerminal> {
        self.notebook()
            .or_else(|| {
                log::warn!("No notebook");
                None
            })
            .and_then(|notebook| {
                let length = notebook.get_n_pages();

                if page_num > length {
                    log::trace!(
                        "page_num ({}) > notebook.get_n_pages() ({})",
                        page_num,
                        length
                    );
                    return None;
                }

                unsafe {
                    let notebook_ref: &Notebook = notebook.as_ref();
                    let notebook_ptr: *mut gtk_sys::GtkNotebook = notebook_ref.to_glib_none().0;
                    let widget_ptr =
                        gtk_sys::gtk_notebook_get_nth_page(notebook_ptr, page_num as i32);

                    if widget_ptr.is_null() {
                        None
                    } else {
                        Some(glib::translate::from_glib_none::<_, gtk::Widget>(
                            widget_ptr,
                        ))
                    }
                }
                .and_then(|widget| widget.downcast::<MtTerminal>().ok())
            })
        // let notebook = self.notebook()?;
        // if page_num < length {
        //     let terminal = unsafe {
        //         let notebook_ref: &Notebook = notebook.as_ref();
        //         let notebook_ptr: *mut gtk_sys::GtkNotebook = notebook_ref.to_glib_none().0;
        //         let widget_ptr = gtk_sys::gtk_notebook_get_nth_page(notebook_ptr, page_num as i32);

        //         glib::translate::from_glib_none::<_, gtk::Widget>(widget_ptr)
        //     }
        //     .downcast::<MtTerminal>()
        //     .expect("Failed to downcast to MtTerminal");

        //     terminal.paste();
        // } else {
        //     log::warn!(
        //         "Requested tab index ({}) >= size of notebook ({})",
        //         page_num,
        //         length
        //     );
        // }
        // let terminal = unsafe {
        //     let notebook_ref: &Notebook = notebook.as_ref();
        //     let notebook_ptr: *mut gtk_sys::GtkNotebook = notebook_ref.to_glib_none().0;
        //     let widget_ptr = gtk_sys::gtk_notebook_get_nth_page(notebook_ptr, page_num as i32);

        //     glib::translate::from_glib_none::<_, gtk::Widget>(widget_ptr)
        // }
        // .downcast::<MtTerminal>();

        // terminal
    }
}

impl WidgetImpl for MtApplicationWindowPriv {}
impl ContainerImpl for MtApplicationWindowPriv {}
impl BinImpl for MtApplicationWindowPriv {}
impl WindowImpl for MtApplicationWindowPriv {}
impl GtkApplicationWindowImpl for MtApplicationWindowPriv {}
impl HdyApplicationWindowImpl for MtApplicationWindowPriv {}

glib::glib_wrapper! {
    pub struct MtApplicationWindow(
        Object<
            subclass::simple::InstanceStruct<MtApplicationWindowPriv>,
            subclass::simple::ClassStruct<MtApplicationWindowPriv>,
            MtApplicationWindowClass
        >
    ) @extends gtk::Widget, gtk::Container, gtk::Bin, gtk::Window, GtkApplicationWindow, HdyApplicationWindow;

    match fn {
        get_type => || MtApplicationWindowPriv::get_type().to_glib(),
    }
}

impl MtApplicationWindow {
    pub fn new(app: &MtApplication) -> Self {
        let window = glib::Object::new(
            Self::static_type(),
            &[("application", &app), ("name", &"terminal-window")],
        )
        .expect("Failed to create MtApplicationWindow")
        .downcast::<MtApplicationWindow>()
        .expect("Created MtApplicationWindow is of wrong type");

        app.add_window(&window);
        window.setup_gactions();

        // This is garbage, its just here to hold things together lol
        // its not permanent I promise. Once I set it up, this will
        // get set by the window event loop
        let colorscheme = app
            .get_default_profile()
            .and_then(|profile| Some(profile.0.colorscheme))
            .expect("lol");

        window
            .get_style_context()
            .add_class(&colorscheme.to_lowercase().replace(" ", "-"));

        window
    }

    pub fn terminal(&self, profile: Option<&str>, cwd: Option<PathBuf>, cmd: Option<Vec<PathBuf>>) {
        let app = self.get_application().unwrap();
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let notebook = priv_.notebook().unwrap();

        let terminal = app
            .clone()
            .downcast::<MtApplication>()
            .ok()
            .and_then(|mtapp| {
                let tparams = profile
                    .and_then(|name| mtapp.get_profile(name))
                    .or(mtapp.get_default_profile())?;

                log::info!("We got this far");
                Some(MtTerminal::new(
                    &self,
                    tparams,
                    cwd,
                    cmd,
                    self.create_sender(),
                ))
            });
        log::info!("terminal {:?}", terminal);

        if let Some(terminal) = terminal {
            let header = MtTabHeader::new();
            terminal.assign_header(&header, &notebook);
            terminal.show();
            header.show();
            notebook.append_page(&terminal, Some(&header));
            notebook.set_tab_reorderable(&terminal, true);
        }
    }

    pub fn close_active_tab(&self) {
        let app = self.get_application().unwrap();
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let notebook = priv_.notebook().unwrap();

        if notebook.get_n_pages() == 1 {
            debug!("Only 1 page open: Quitting App");
            app.quit();
        } else if let Some(page_num) = notebook.get_current_page() {
            notebook.remove_page(Some(page_num));
            debug!("Closing page {}", page_num);
        }
    }

    pub fn focus_active_tab(&self) {
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let notebook = priv_.notebook().unwrap();

        if notebook.get_n_pages() > 0 {
            if let Some(page) = notebook.get_current_page() {
                notebook.get_children()[page as usize]
                    .downcast_ref::<MtTerminal>()
                    .expect("Notebook page is of wrong type")
                    .focus();
            }
        }
    }

    pub fn set_tab_bar_visible(&self, show_tabs_titlebar: bool) {
        let priv_ = MtApplicationWindowPriv::from_instance(self);

        if *priv_.tabs_visible.borrow() != show_tabs_titlebar {
            let notebook = priv_.notebook().unwrap();
            let windowhandle = priv_.windowhandle().unwrap();
            let layout = priv_.layout.borrow();

            if *layout == Layout::Unified {
                notebook.set_show_tabs(show_tabs_titlebar);
                windowhandle.set_visible(!show_tabs_titlebar);
            }

            priv_.tabs_visible.replace(show_tabs_titlebar);
        }
    }

    pub fn get_tab_bar_visible(&self) -> bool {
        let priv_ = MtApplicationWindowPriv::from_instance(self);

        priv_.tabs_visible.borrow().clone()
    }

    pub fn set_titles(&self, title: &str) {
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let secondary_titlebar = priv_
            .titlebar()
            .expect("Failed to to get secondary-titlebar, should not occur");

        self.set_title(title);
        secondary_titlebar
            .get_custom_title()
            .expect("Failed to get title")
            .downcast_ref::<Label>()
            .expect("Custom title is of wrong type")
            .set_text(title);
    }

    fn setup_gactions(&self) {
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        let window = self.clone().upcast::<gtk::ApplicationWindow>();
        let notebook = priv_
            .notebook()
            .expect("Failed to get notebook, should not occur");
        let sender = self.create_sender();

        util::action(
            &window,
            "new-tab",
            clone!(@weak self as window => move |_, _| {
                window.terminal(None, None, None);
            }),
            None,
        );

        util::action(
            &window,
            "close-active-tab",
            clone!(@weak self as window => move |_, _| {
                window.close_active_tab();
            }),
            None,
        );

        util::action(
            &window,
            "clipboard-copy",
            clone!(@strong sender, @weak notebook => move |_, _| {
                let page = unsafe {
                    let notebook_ref: &Notebook = notebook.as_ref();
                    let notebook_ptr: *mut gtk_sys::GtkNotebook = notebook_ref.to_glib_none().0;

                    u32::try_from(gtk_sys::gtk_notebook_get_current_page(notebook_ptr)).ok()
                };
                if let Some(page) = page {
                    let _ = sender.send(WindowCommand::Clipboard { action: Clipboard::Copy, page });
                }
            }),
            None,
        );

        util::action(
            &window,
            "clipboard-paste",
            clone!(@strong sender, @weak notebook => move |_, _| {
                let page = unsafe {
                    let notebook_ref: &Notebook = notebook.as_ref();
                    let notebook_ptr: *mut gtk_sys::GtkNotebook = notebook_ref.to_glib_none().0;

                    u32::try_from(gtk_sys::gtk_notebook_get_current_page(notebook_ptr)).ok()
                };
                if let Some(page) = page {
                    let _ = sender.send(WindowCommand::Clipboard { action: Clipboard::Paste, page });
                }
            }),
            None,
        );

        for i in 1..11 {
            util::action(
                &window,
                &format!("select-tab-{}", i),
                clone!(@weak notebook => move |_, _| {
                    notebook.set_property_page(i - 1);
                }),
                None,
            );
            debug!("Connecting GAction: app.select-tab-{}", i);
        }
    }

    pub fn create_sender(&self) -> Sender<WindowCommand> {
        let priv_ = MtApplicationWindowPriv::from_instance(self);
        priv_.create_sender()
    }
}

pub fn about_dialog<T: IsA<Window>>(parent: &T, app: &Application) {
    let dialog = AboutDialogBuilder::new()
        .transient_for(parent)
        .application(app)
        .deletable(true)
        .decorated(true)
        .program_name(crate::app::APPID)
        .artists(vec!["Michael Ditto".to_string()])
        .authors(vec!["Michael Ditto".to_string()])
        .version(env!("CARGO_PKG_VERSION"))
        .license_type(License::Gpl20Only)
        .logo_icon_name(&crate::app::APPID)
        .modal(true)
        .build();

    match dialog.run() {
        _ => dialog.emit_close(),
    }
}
