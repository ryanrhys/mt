use std::process::Command;

fn main() {
    Command::new("glib-compile-resources")
        .current_dir("data")
        .arg("--target=../target/com.gitlab.miridyan.Mt.gresource")
        .arg("com.gitlab.miridyan.Mt.gresource.xml")
        .output()
        .expect("failed to build gresource");
}
